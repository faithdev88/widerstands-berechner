package de.faith.aufgabe5.calculate;

import de.faith.aufgabe5.resistor.Resistor;
import de.faith.aufgabe5.resistor.TotalResistance;

import java.io.*;
import java.util.*;

public class CalculateResistor {

    private final TotalResistance totalResistance;
    private final List<Resistor> allResistors;
    private final List<Integer> resistors;
    private final Scanner scanner;
    private final double requiredResistance;
    private double bestResistor;
    private boolean keepSearching;

    /*
    @param requiredResistance Benötigter Widerstand
    @param scanner Eingabe von Benutzer
     */
    public CalculateResistor(double requiredResistance, Scanner scanner) {
        totalResistance = new TotalResistance();
        allResistors = new ArrayList<>();
        resistors = new ArrayList<>();
        bestResistor = Double.MAX_VALUE;
        keepSearching = true;
        this.requiredResistance = requiredResistance;
        this.scanner = scanner;
    }

    //finde den benötigten Widerstand
    public void findResistor() {
        loadResistors();
        calculateResistor();
        for (Resistor r : allResistors) {
            if (r.getTotalResistor() == requiredResistance) {
                totalResistance.setTotalResistance(r);
                System.out.println("\nEs wurde ein passender Widerstand gefunden!");
                System.out.println("MÃ¶chten Sie weiter suchen um zu Ã¼berprÃ¼fen ob es eine effizentere Schaltung gibt?");
                String line = "";
                while (!line.equalsIgnoreCase("Ja") && !line.equalsIgnoreCase("Nein")) {
                    System.out.println("Bitte geben Sie Ja oder Nein ein.");
                    line = scanner.nextLine();
                }
                if (line.equalsIgnoreCase("Nein")) {
                    keepSearching = false;
                    break;
                }
            } else {
                double temp = bestResistor;
                bestResistor = Math.min(Math.abs((r.getTotalResistor()) - requiredResistance), bestResistor);
                if (bestResistor != temp) {
                    totalResistance.setTotalResistance(r);
                }
            }
        }
        if (keepSearching) {
            if (totalResistance.getTotalResistor() != requiredResistance) {
                allResistors.parallelStream().forEach(r -> allResistors.forEach(r2 -> {
                    if (r.isSeries() && !r2.isSeries() && ((r.getResistors().length + r2.getResistors().length) <= 4)) {
                        boolean isDouble = Arrays.stream(r.getResistors()).anyMatch(resistor -> Arrays.stream(r2.getResistors()).anyMatch(resistor2 -> resistor == resistor2));
                        double temp = bestResistor;
                        if (!isDouble) {
                            if ((r.getTotalResistor() + r2.getTotalResistor()) == requiredResistance) {
                                totalResistance.setTotalResistance(r, r2);
                            } else {
                                bestResistor = Math.min(Math.abs((r.getTotalResistor() + r2.getTotalResistor()) - requiredResistance), bestResistor);
                                if (bestResistor != temp) {
                                    totalResistance.setTotalResistance(r, r2);
                                }
                            }
                        }
                    }
                }));
            }
        }
        if (requiredResistance != totalResistance.getTotalResistor()) {
            System.out.println("Es wurde kein passender Widerstand gefunden");
            System.out.println("Der Widerstand der am nÃ¤chsten dran ist hat ein Widerstandswert von: " + totalResistance.getTotalResistor() + " Î©");
            System.out.println("Der Bauplan lautet: " + totalResistance.getBluePrint());
        } else {
            System.out.println("Es wurde ein passender Widerstand gefunden");
            System.out.println("Der Widerstand hat: " + totalResistance.getTotalResistor() + " Î©");
            System.out.println("Der Bauplan lautet: " + totalResistance.getBluePrint());
        }

    }

    //Berrechnent alle Parallel und Reihenschaltungen
    private void calculateResistor() {
        System.out.println("Erstelle Parallel und Reihenschaltungen");
        for (double r1 : resistors) {
            if (requiredResistance == r1) {
                allResistors.add(new Resistor(true, new double[]{r1}));
                break;
            }
            for (double r2 : resistors) {
                if (r1 == r2) {
                    continue;
                }
                allResistors.add(new Resistor(true, new double[]{r1, r2}));
                allResistors.add(new Resistor(false, new double[]{r1, r2}));
                for (double r3 : resistors) {
                    if (r3 == r2 || r3 == r1) {
                        continue;
                    }
                    allResistors.add(new Resistor(true, new double[]{r1, r2, r3}));
                    allResistors.add(new Resistor(false, new double[]{r1, r2, r3}));
                    for (double r4 : resistors) {
                        if (r4 == r1 || r4 == r2 || r4 == r3) {
                            continue;
                        }
                        allResistors.add(new Resistor(true, new double[]{r1, r2, r3, r4}));
                        allResistors.add(new Resistor(false, new double[]{r1, r2, r3, r4}));
                    }
                }
            }
        }
        optimizeResistor();
    }

    //Entfernt alle Doppelten Widerstände die unnötig sind
    private void optimizeResistor() {
        Map<Double, Resistor> optimizedResistor = new HashMap<>();
        System.out.println("Optimiere die Schaltungen fÃ¼r bessere Performance");
        for (Resistor r : allResistors) {
            if (optimizedResistor.containsKey(r.getTotalResistor())) {
                if (optimizedResistor.get(r.getTotalResistor()).getResistors().length > r.getResistors().length) {
                    optimizedResistor.put(r.getTotalResistor(), r);
                }
            } else {
                optimizedResistor.put(r.getTotalResistor(), r);
            }
        }
        allResistors.clear();
        allResistors.sort(Comparator.comparingDouble(Resistor::getTotalResistor));
        allResistors.addAll(optimizedResistor.values());
    }

    // Lade die Widerstände in der Widerstände.txt Datei
    private void loadResistors() {
        File file = new File("Widerstände.txt");
        BufferedReader br;
        String line;
        try {
            if (!file.exists()) {
                System.out.println("Die Datei konnte nicht gefunden werden");
                System.out.println("Bitte packen Sie alle WiderstÃ¤nde in der WinderstÃ¤nde.txt Datei oder benennen Sie ihre Datei nach WiderstÃ¤nde.txt und ziehen diese in den Ordner,\n wo das Prgramm gestartet wird");
                file.createNewFile();
                System.exit(0);
            }
            br = new BufferedReader(new FileReader(file));
            System.out.println("WiderstÃ¤nde werden geladen");
            while ((line = br.readLine()) != null) {
                resistors.add(Integer.parseInt(line));
            }
            if (resistors.isEmpty()) {
                System.out.println("Es wurden keine WiderstÃ¤nde gefunden, die Datei ist leer");
                System.exit(0);
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
