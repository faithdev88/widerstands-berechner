package de.faith.aufgabe5;

import java.util.Scanner;

import de.faith.aufgabe5.calculate.CalculateResistor;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        CalculateResistor resistor;
        String line;
        double requiredResistance;

        System.out.println("Bitte geben Sie den gesuchten Widerstand ein.");
        line = scanner.nextLine();
        while (!line.matches("[\\d+.]*")) {
            System.err.println("Bitte geben Sie nur Zahlen ein die im Positiven bereich sind");
            System.out.println("Bitte geben Sie den gesuchten Widerstand ein.");
            line = scanner.nextLine();
        }
        requiredResistance = Double.parseDouble(line);

        resistor = new CalculateResistor(requiredResistance, scanner);
        resistor.findResistor();
        scanner.close();
    }
}
