Calculates the total resistance with given resistances.
<br>This is a bwinf task see: https://git.bwinf.de/bwinf/bwinf37-runde1/tree/master/a5-Widerstand
<br>Release 1.0: https://gitlab.com/faithdev88/widerstands-berechner/tags